//
//  main.m
//  O8-MutableStrings
//
//  Created by Helder Pereira on 09/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSString *teste = @"isto é um teste";
//substring
    
//    NSRange range;
//    range.location = 2;
//    range.length = 6;

    NSRange range = NSMakeRange(2, 6);
    
    NSString *nova = [teste substringWithRange:range];
    
    NSLog(@"%@", nova);
    
    NSMutableString *mutie = [NSMutableString stringWithString:@"valor inicial"];
    
    [mutie appendString:@"ola"];
    [mutie appendString:@"ola"];
    [mutie replaceCharactersInRange:NSMakeRange(0, 9) withString:@"V"];
    
    NSLog(@"%@", mutie);
    
    NSString *fruta = @"banana";
    // b-a-n-a-n-a
    
    NSMutableString *novaFruta = [[NSMutableString alloc] init];
    
    for(int i = 0; i < fruta.length; i++) {
        
//        NSString *strLetra = [fruta substringWithRange:NSMakeRange(i, 1)];
//        [novaFruta appendString:strLetra];

        if (i > 0) {
            [novaFruta appendString:@"-"];
        }
        
        unichar letra = [fruta characterAtIndex:i];
        [novaFruta appendFormat:@"%c", letra];
    }
    
    NSLog(@"%@", novaFruta);
    
    return 0;
}


