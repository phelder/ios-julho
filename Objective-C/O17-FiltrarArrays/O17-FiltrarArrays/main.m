//
//  main.m
//  O17-FiltrarArrays
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray<Pessoa *> *gente = @[
                   [Pessoa pessoaWithNome:@"André" idade:10 cidade:@"Porto"],
                   [Pessoa pessoaWithNome:@"Xana" idade:12 cidade:@"Lisboa"],
                   [Pessoa pessoaWithNome:@"Tiago" idade:30 cidade:@"Faro"],
                   [Pessoa pessoaWithNome:@"Gigi" idade:32 cidade:@"Porto"],
                   [Pessoa pessoaWithNome:@"Emanuel" idade:40 cidade:@"Porto"],
                   [Pessoa pessoaWithNome:@"Lopes" idade:41 cidade:@"Aveiro"],
                   [Pessoa pessoaWithNome:@"Manel" idade:3 cidade:@"Porto"],
                   [Pessoa pessoaWithNome:@"NO" idade:9 cidade:@"Lisboa"]
                       ];
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cidade = [cd]%@ AND idade > %d", @"porto", 20];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nome CONTAINS [cd]%@", @"e"];
    
    NSArray<Pessoa *> *genteFiltrada = [gente filteredArrayUsingPredicate:predicate];
    
    NSLog(@"%@", genteFiltrada);
    
    return 0;
}
