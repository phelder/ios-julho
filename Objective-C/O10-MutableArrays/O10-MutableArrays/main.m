//
//  main.m
//  O10-MutableArrays
//
//  Created by Helder Pereira on 09/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSMutableArray *cenas = [[NSMutableArray alloc] init];
    
    NSNumber *n1 = [NSNumber numberWithInt:34];
    
    NSArray *numeros = @[
                         [NSNumber numberWithInt:15],
                         [NSNumber numberWithInt:25],
                         [NSNumber numberWithInt:35],
                         [NSNumber numberWithInt:45],
                         [NSNumber numberWithInt:55]
                         ];
    
    NSNumber *n2 = @(34);
    
    NSArray *maisNumeros = @[
                             @(34),
                             @(1),
                             @(16),
                             @(65),
                             @(6),
                             @(76),
                             @(34),
                             ];
    
    NSMutableArray <NSNumber *> *ns = [NSMutableArray arrayWithArray:maisNumeros];
    
    for (int i = 0; i < ns.count; i++) {
        
        if (ns[i].intValue < 18) {
            [ns removeObjectAtIndex:i];
            i--;
        }
    }
    
    NSLog(@"%@", ns);
    
    
    return 0;
}
