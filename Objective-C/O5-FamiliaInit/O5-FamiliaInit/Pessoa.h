//
//  Pessoa.h
//  O5-FamiliaInit
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (assign, nonatomic) NSUInteger idade;
@property (strong, nonatomic) NSString *cidade;

// - iniWithNome:
- (instancetype)initWithNome:(NSString *)nome;

// - initWithNome:idade:cidade:
- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade;

@end
