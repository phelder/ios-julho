//
//  main.m
//  O5-FamiliaInit
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    Pessoa *p2 = [[Pessoa alloc] initWithNome:@"Tobias"];
    Pessoa *p3 = [[Pessoa alloc] initWithNome:@"Tobias" idade:20 cidade:@"Porto"];
    
    
    NSLog(@"%@", p1.nome);
    NSLog(@"%@", p2.nome);
    NSLog(@"%@", p3.nome);
    
    return 0;
}
