//
//  Pessoa.m
//  O5-FamiliaInit
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSLog(@"YAY UMA PESSOA...");
        
//        _nome = @"qqcoisa";
//        [self setNome: @"qqcoisa"];
//        self.nome = @"qqcoisa";
        
        self.nome = @"nome por defeito";
        self.idade = 0;
        self.cidade = @"cidade por defeito";
    }
    return self;
}

- (instancetype)initWithNome:(NSString *)nome
{
    self = [self init];
    if (self) {
        self.nome = nome;
    }
    return self;
}

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        self.nome = nome;
        self.idade = idade;
        self.cidade = cidade;
    }
    return self;
}

@end
