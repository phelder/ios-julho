//
//  main.m
//  O14-BitOperations
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Teste.h"

int main(int argc, const char * argv[]) {
    
    int resAnd = 7 & 11;
    int resOr = 7 | 11;
    
    int valor = 1 << 5;
    
    NSLog(@"%d", resAnd);
    NSLog(@"%d", resOr);
    NSLog(@"%d", valor);
    
    Teste *t = [[Teste alloc] init];
    
    [t recebeOpcoes:Option1|Option3];
    
    return 0;
}


