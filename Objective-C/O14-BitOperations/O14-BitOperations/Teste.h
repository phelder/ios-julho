//
//  Teste.h
//  O14-BitOperations
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    OptionNone = 0,
    Option1 = 1 << 0,
    Option2 = 1 << 1,
    Option3 = 1 << 2,
    Option4 = 1 << 3
} Options;

@interface Teste : NSObject

- (void)recebeOpcoes:(Options)options;

@end
