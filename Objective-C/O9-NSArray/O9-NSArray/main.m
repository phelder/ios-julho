//
//  main.m
//  O9-NSArray
//
//  Created by Helder Pereira on 09/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSArray *array1 = [[NSArray alloc] initWithObjects:@"a", @"b", @"c", nil];
    
    for (int i = 0; i < array1.count; i++) {
        
        NSLog(@"%@", [array1 objectAtIndex:i]);
        
    }
    
    NSArray *array2 = @[
                        @"Helder",
                        @"Diogo",
                        @"Vítor",
                        @"Pedro",
                        @"Tiago",
                        @"Nsimba"
                        ];
    
    for (int i = 0; i < array2.count; i++) {
        
        NSLog(@"%@", array2[i]);
        
    }
    
    NSString *malta = [array2 componentsJoinedByString:@", "];
    
    NSLog(@"%@", malta);
    
    return 0;
}
