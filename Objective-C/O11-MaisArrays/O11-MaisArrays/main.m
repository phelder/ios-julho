//
//  main.m
//  O11-MaisArrays
//
//  Created by Helder Pereira on 09/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray<Pessoa *> *malta = @[
                                 [Pessoa pessoaWithNome:@"A" idade:18 cidade:@"Porto"],
                                 [Pessoa pessoaWithNome:@"B" idade:21 cidade:@"Lisboa"],
                                 [Pessoa pessoaWithNome:@"C" idade:45 cidade:@"Porto"],
                                 [Pessoa pessoaWithNome:@"D" idade:45 cidade:@"Lisboa"],
                                 [Pessoa pessoaWithNome:@"E" idade:12 cidade:@"Porto"],
                                 [Pessoa pessoaWithNome:@"F" idade:10 cidade:@"Lisboa"],
                                 [Pessoa pessoaWithNome:@"G" idade:30 cidade:@"Lisboa"],
                                 [Pessoa pessoaWithNome:@"H" idade:5 cidade:@"Porto"],
                                 [Pessoa pessoaWithNome:@"I" idade:65 cidade:@"Porto"],
                                 [Pessoa pessoaWithNome:@"Vítor" idade:32 cidade:@"Trofa"],
                                 [Pessoa pessoaWithNome:@"K" idade:10 cidade:@"Porto"],
                                 ];

    NSMutableArray<Pessoa *> *novos = [[NSMutableArray alloc] init];
    NSMutableArray<Pessoa *> *velhos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < malta.count; i++) {
        Pessoa *p = malta[i];
        
        if (p.idade < 18) {
            [novos addObject:p];
        } else {
            [velhos addObject:p];
        }
    }
    
    NSLog(@"NOVOS: %@", novos);
    NSLog(@"VELHOS: %@", velhos);
    
    Pessoa *p10 = malta[9];
    
    NSLog(@"%@", p10);
    
    
    return 0;
}
