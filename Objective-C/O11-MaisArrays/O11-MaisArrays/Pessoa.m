//
//  Pessoa.m
//  O11-MaisArrays
//
//  Created by Helder Pereira on 09/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        self.nome = nome;
        self.idade = idade;
        self.cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"%@ - %lu - %@", self.nome, (unsigned long)self.idade, self.cidade];
    
}

@end
