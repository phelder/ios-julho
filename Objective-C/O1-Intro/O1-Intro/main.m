//
//  main.m
//  O1-Intro
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
   
    int a = 10;
    int b = 15;
    
    int soma = a + b;
    
    
    NSLog(@"Soma de %d com %d é %d", a, b, soma);
    
    
    NSString *texto = @"teste";
    
    NSLog(@"isto é um %@", texto);
    
    return 0;
}
