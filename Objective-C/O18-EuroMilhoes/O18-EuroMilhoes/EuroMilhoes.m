//
//  EuroMilhoes.m
//  O18-EuroMilhoes
//
//  Created by Helder Pereira on 16/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "EuroMilhoes.h"

@implementation EuroMilhoes
{
    NSArray<NSNumber *> *_numeros;
    NSArray<NSNumber *> *_estrelas;
}

+ (NSInteger)randomizaEntre:(NSInteger)min e:(NSInteger)max {
    if (min > max) {
        return 0;
    }
    return arc4random() % (max - min + 1) + min;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self geraChave];
    }
    return self;
}

- (void)geraChave {
    
    NSMutableArray *numeros = [[NSMutableArray alloc] init];
    NSMutableArray *estrelas = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 5; i++) {
        NSNumber *novoNumero;
        do {
            novoNumero = @([EuroMilhoes randomizaEntre:1 e:50]);
        } while ([numeros containsObject:novoNumero]);
        [numeros addObject:novoNumero];
    }
    
    for (int i = 0; i < 2; i++) {
        NSNumber *novoNumero;
        do {
            novoNumero = @([EuroMilhoes randomizaEntre:1 e:11]);
        } while ([estrelas containsObject:novoNumero]);
        [estrelas addObject:novoNumero];
    }
    
    _numeros = [numeros sortedArrayUsingSelector:@selector(compare:)];
    _estrelas = [estrelas sortedArrayUsingSelector:@selector(compare:)];
}

- (NSArray *)numeros {
    return _numeros;
}

- (NSArray *)estrelas {
    return _estrelas;
}

- (NSString *)numerosString {
//    NSMutableString *numerosString = [[NSMutableString alloc] init];
//    
//    for (int i = 0; i < _numeros.count; i++) {
//        if (i > 0) {
//            [numerosString appendString:@", "];
//        }
//        [numerosString appendString:_numeros[i].description];
//    }
//    
//    return [NSString stringWithString:numerosString];
    return [_numeros componentsJoinedByString:@", "];
}

- (NSString *)estrelasString {
    return [_estrelas componentsJoinedByString:@", "];
}

- (NSString *)chave {
    return [NSString stringWithFormat:@"%@ - %@", self.numerosString, self.estrelasString];
}

- (NSString *)description {
    return self.chave;
}

@end
