//
//  EuroMilhoes2.m
//  O18-EuroMilhoes
//
//  Created by Helder Pereira on 16/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "EuroMilhoes2.h"

#define NUMEROS_MIN 1
#define NUMEROS_MAX 50
#define NUMEROS_CASAS 5
#define ESTRELAS_MIN 1
#define ESTRELAS_MAX 11
#define ESTRELAS_CASAS 2

@implementation EuroMilhoes2

+ (NSInteger)randomizaEntre:(NSInteger)min e:(NSInteger)max {
    if (min > max) {
        return 0;
    }
    return arc4random() % (max - min + 1) + min;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self geraChave];
    }
    return self;
}

- (void)geraChave {
    
    NSMutableArray *numeros = [[NSMutableArray alloc] init];
    NSMutableArray *estrelas = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < NUMEROS_CASAS; i++) {
        NSNumber *novoNumero;
        do {
            novoNumero = @([EuroMilhoes2 randomizaEntre:NUMEROS_MIN e:NUMEROS_MAX]);
        } while ([numeros containsObject:novoNumero]);
        [numeros addObject:novoNumero];
    }
    
    for (int i = 0; i < ESTRELAS_CASAS; i++) {
        NSNumber *novoNumero;
        do {
            novoNumero = @([EuroMilhoes2 randomizaEntre:ESTRELAS_MIN e:ESTRELAS_MAX]);
        } while ([estrelas containsObject:novoNumero]);
        [estrelas addObject:novoNumero];
    }
    
    _numeros = [numeros sortedArrayUsingSelector:@selector(compare:)];
    _estrelas = [estrelas sortedArrayUsingSelector:@selector(compare:)];
    
    _numerosString = [_numeros componentsJoinedByString:@", "];
    _estrelasString = [_estrelas componentsJoinedByString:@", "];
    _chave = [NSString stringWithFormat:@"%@ - %@", _numerosString, _estrelasString];
}

- (NSString *)description {
    return self.chave;
}

- (NSComparisonResult)compare:(EuroMilhoes2 *)otherObject {
    return [self.chave compare:otherObject.chave];
}

@end
