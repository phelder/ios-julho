//
//  main.m
//  O18-EuroMilhoes
//
//  Created by Helder Pereira on 16/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EuroMilhoes2.h"

int main(int argc, const char * argv[]) {
    
    EuroMilhoes2 *chaveOriginal = [[EuroMilhoes2 alloc] init];
    
    int contador = 0;
    
    while (true) {
        
        contador ++;
        
        EuroMilhoes2 *novaChave = [[EuroMilhoes2 alloc] init];
        
        if ([novaChave compare:chaveOriginal] == NSOrderedSame) {
            break;
        }
        
        if (contador % 10000 == 0) {
            NSLog(@"%d", contador);
        }
    }
    
    NSLog(@"ENCONTREI EM %d TENTATIVAS", contador);
    
    
    return 0;
}
