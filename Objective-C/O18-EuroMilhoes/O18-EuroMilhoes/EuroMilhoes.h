//
//  EuroMilhoes.h
//  O18-EuroMilhoes
//
//  Created by Helder Pereira on 16/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EuroMilhoes : NSObject

// - (instancetype) init;
// - (void)geraChave;

- (NSArray *)numeros;
- (NSArray *)estrelas;

- (NSString *)numerosString; // 2, 5, 12, 30, 34
- (NSString *)estrelasString; // 2, 11

- (NSString *)chave; // 2, 5, 12, 30, 34 - 2, 11
// - (NSString *)description;

@end
