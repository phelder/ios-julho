//
//  main.m
//  O12-Input
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
 
    char input[255];
    
    NSLog(@"Como te chamas?");
    
    fgets(input, 255, stdin);
    input[strlen(input) -1] = '\0';
    
    NSString *nome = [NSString stringWithCString:input encoding:NSUTF8StringEncoding];
    
    NSLog(@"Olá %@", nome);
    
    
    return 0;
}
