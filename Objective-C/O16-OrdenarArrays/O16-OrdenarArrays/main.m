//
//  main.m
//  O16-OrdenarArrays
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray *nomes = @[
                       @"1000",
                       @"Zé",
                       @"Ana",
                       @"24"
                       ];
    
    NSLog(@"%@", nomes);
    
    NSArray *nomesOrdenados = [nomes sortedArrayUsingSelector:@selector(compare:)];
    
    NSLog(@"%@", nomesOrdenados);
    
    NSArray *numeros = @[
                         @(10),
                         @(100),
                         @(21),
                         @(34),
                         @(5)
                         ];
    
    NSArray *numsOrdenados = [numeros sortedArrayUsingSelector:@selector(compare:)];
    
    NSLog(@"%@", numsOrdenados);
    
    
    
    NSArray *gente = @[
                       [Pessoa pessoaWithNome:@"A" idade:10 cidade:@"P"],
                       [Pessoa pessoaWithNome:@"X" idade:12 cidade:@"L"],
                       [Pessoa pessoaWithNome:@"T" idade:30 cidade:@"F"],
                       [Pessoa pessoaWithNome:@"G" idade:32 cidade:@"P"],
                       [Pessoa pessoaWithNome:@"ABC" idade:40 cidade:@"P"],
                       [Pessoa pessoaWithNome:@"L" idade:41 cidade:@"A"],
                       [Pessoa pessoaWithNome:@"M" idade:3 cidade:@"P"],
                       [Pessoa pessoaWithNome:@"NO" idade:9 cidade:@"L"],
                       ];
    
    NSLog(@"%@", gente);
    
    NSArray *genteOrdenada = [gente sortedArrayUsingSelector:@selector(compareByNome:)];
    
    NSLog(@"%@", genteOrdenada);
    

    NSArray<NSSortDescriptor *> *descriptors = @[
                [NSSortDescriptor sortDescriptorWithKey:@"cidade" ascending:YES],
                [NSSortDescriptor sortDescriptorWithKey:@"idade" ascending:YES],
                                                ];
    
    genteOrdenada = [gente sortedArrayUsingDescriptors:descriptors];

    NSLog(@"%@", genteOrdenada);

    
    return 0;
}
