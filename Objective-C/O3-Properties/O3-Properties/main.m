//
//  main.m
//  O3-Properties
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    
    //[p1 setNome:@"Helder"];
    p1.nome = @"Helder";
    p1.idade = 35;
    p1.cidade = @"Porto";
    
    Pessoa *p2 = [[Pessoa alloc] init];
    p2.nome = @"Ze";
    p2.idade = 40;
    p2.cidade = @"Porto";
    
    if (p1.idade > p2.idade) {
    
        NSLog(@"Mais velho: %@ - %d - %@", p1.nome, p1.idade, p1.cidade);
        
    } else {
        
        NSLog(@"Mais velho: %@ - %d - %@", p2.nome, p2.idade, p2.cidade);
        
    }
    
    return 0;
}
