//
//  Pessoa.m
//  O2-Objects
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa
{
    NSString * _nome;
    int        _idade;
    NSString * _cidade;
}

- (NSString *)nome {
    return _nome;
}

- (int)idade {
    return _idade;
}

- (NSString *)cidade {
    return _cidade;
}

- (void)setNome:(NSString *)nome {
    _nome = nome;
}

- (void)setIdade:(int)idade {
    
    NSLog(@"YAY SET IDADE");
    
    _idade = idade;
}

- (void)setCidade:(NSString *)cidade {
    _cidade = cidade;
}

// function salta() { ... }
// void salta() { ... }
- (void)salta {
    
    NSLog(@"ELE(A) SALTOU!!!!!");
    
}

- (int)recebeUmInteiro:(int)numero {
    return numero * 2;
}

- (int)recebeUmInteiro:(int)numero :(int)outroNumero {
    
    return numero + outroNumero;
    
}

- (int)recebeUmInteiro:(int)numero eOutro:(int)outroNumero eMaisUm:(int)aindaOutroNumero {

    return (numero + outroNumero) * aindaOutroNumero;
    
}

@end
