//
//  main.m
//  O2-Objects
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
   
    // Pessoa p1 = new Pessoa();
//    Pessoa *p1 = [[Pessoa alloc] init];
//    
//    [p1 salta];
//    
//    [p1 recebeUmInteiro:12];
//    [p1 recebeUmInteiro:12 :34];
//    [p1 recebeUmInteiro:12 eOutro:34 eMaisUm:50];
    

    Pessoa *p1 = [[Pessoa alloc] init];
    [p1 setNome:@"Tobias"];
    p1.idade = 12;
    [p1 setCidade:@"Porto"];
    
    NSLog(@"Nome: %@", p1.nome);
    
    // DOT SYNTAX...
    
    p1.nome = @"Tomás";
    [p1 nome];
    
    return 0;
}
