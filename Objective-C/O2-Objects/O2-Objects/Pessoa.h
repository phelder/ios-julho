//
//  Pessoa.h
//  O2-Objects
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

// GETTERS
- (NSString *)nome;
- (int)idade;
- (NSString *)cidade;

// SETTERS
- (void)setNome:(NSString *)nome;
- (void)setIdade:(int)idade;
- (void)setCidade:(NSString *)cidade;

- (void)salta;
- (int)recebeUmInteiro:(int)numero;
- (int)recebeUmInteiro:(int)numero :(int)outroNumero; // XUNGA

- (int)recebeUmInteiro:(int)numero eOutro:(int)outroNumero eMaisUm:(int)aindaOutroNumero;

@end
