//
//  main.m
//  O19-LerEscreverFicheiros
//
//  Created by Helder Pereira on 16/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

void escreverFicheiros() {
    NSFileManager *manager = [NSFileManager defaultManager];
    
    NSArray <NSURL *> *urls = [manager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask];
    
    NSURL *desktopURL = urls[0];
    
    NSURL *fileURL = [desktopURL URLByAppendingPathComponent:@"Teste/ola2.txt"];
    
    [@"afinal o texto é outro" writeToURL:fileURL atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
}

void lerFicheiros() {
    NSFileManager *manager = [NSFileManager defaultManager];
    
    NSArray <NSURL *> *urls = [manager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask];
    
    NSURL *desktopURL = urls[0];
    
    NSURL *fileURL = [desktopURL URLByAppendingPathComponent:@"Teste/ola.txt"];
    
    NSError *error = nil;
    NSString *fileContent = [NSString stringWithContentsOfURL:fileURL encoding:NSUTF8StringEncoding error:&error];
    
    if (error) {
        NSLog(@"%@", error);
    } else {
        NSLog(@"FILE CONTENT:\n%@", fileContent);
    }
}

int main(int argc, const char * argv[]) {
    
    escreverFicheiros();
    
    return 0;
}
