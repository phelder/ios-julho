//
//  main.m
//  O15-Compare
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    
    NSString *s1 = @"Ladida";
    NSString *s2 = @"PIKACHU";
    
    [s1 compare:s2]; // retorna ASC, SAME ou DESC
    
    NSNumber *n1 = @(23);
    NSNumber *n2 = @(65);
    
    [n1 compare:n2]; // retorna ASC, SAME ou DESC
    
    Pessoa *p1 = [Pessoa pessoaWithNome:@"Tobias" idade:30 cidade:@"Porto"];
    Pessoa *p2 = [Pessoa pessoaWithNome:@"Tomás" idade:35 cidade:@"Lisboa"];
    
    if ([p1 compareByIdade:p2] == NSOrderedSame) {
        NSLog(@"Têm a mesma idade...");
    }
    
    return 0;
}
