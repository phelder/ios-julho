//
//  Pessoa.m
//  O15-Compare
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        self.nome = nome;
        self.idade = idade;
        self.cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade {

    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
    
}

- (NSComparisonResult)compareByNome:(Pessoa *)outraPessoa {
    
    return [self.nome compare:outraPessoa.nome options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch];
    
}

- (NSComparisonResult)compareByIdade:(Pessoa *)outraPessoa {

//    if (self.idade > outraPessoa.idade) {
//        return NSOrderedDescending;
//    }
//
//    if (self.idade < outraPessoa.idade) {
//        return NSOrderedAscending;
//    }
//
//    return NSOrderedSame;
    
    return [@(self.idade) compare:@(outraPessoa.idade)];
}

- (NSComparisonResult)compareByCidade:(Pessoa *)outraPessoa {
    
    return [self.cidade compare:outraPessoa.cidade options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch];
    
}

@end
