//
//  main.m
//  O6-MetodosClasse
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Teste.h"
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSString *nome = @"Tobias";
    NSString *apelido = @"Manuel";
    
    NSString *nomeCompleto = [[NSString alloc] initWithFormat:@"%@ %@", nome, apelido];
    NSString *boasVindas = [[NSString alloc] initWithFormat:@"Olá %@", nome];
    
    NSString *novaString = [NSString stringWithFormat:@"Olá %@", nomeCompleto];
    
    NSLog(@"%@", novaString);
    
    
    Pessoa *p1 = [[Pessoa alloc] initWithNome:@"Helder" idade:35 cidade:@"Porto"];
   
    Pessoa *p2 = [Pessoa pessoaWithNome:@"Tobias" idade:30 cidade:@"Lisboa"];
  
    
    
    
    return 0;
}
