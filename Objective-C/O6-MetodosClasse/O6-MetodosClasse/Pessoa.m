//
//  Pessoa.m
//  O6-MetodosClasse
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        self.nome = nome;
        self.idade = idade;
        self.cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
//    Pessoa *p = [[Pessoa alloc] init];
//    p.nome = nome;
//    p.idade = idade;
//    p.cidade = cidade;
//    return p;
    
//    Pessoa *p = [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
//    return p;
    
    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
}

@end
