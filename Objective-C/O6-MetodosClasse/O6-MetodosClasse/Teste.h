//
//  Teste.h
//  O6-MetodosClasse
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Teste : NSObject

@property (assign, nonatomic) int numero;

+ (void)teste;

@end
