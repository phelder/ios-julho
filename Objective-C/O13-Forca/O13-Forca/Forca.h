//
//  Forca.h
//  O13-Forca
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Forca : NSObject

- (instancetype)initWithFrase:(NSString *)frase;
- (instancetype)initWithFrases:(NSArray<NSString *> *)frases;

- (NSString *)fraseJogo;
- (NSUInteger)vidas;

- (BOOL)verificaSeExisteLetra:(NSString *)letra;

- (BOOL)emJogo;
- (BOOL)ganhou;

@end
