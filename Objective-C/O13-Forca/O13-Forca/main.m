//
//  main.m
//  O13-Forca
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Forca.h"

int main(int argc, const char * argv[]) {
    
    // LER FICHEIRO
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *frasesURL = [[fileManager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask][0] URLByAppendingPathComponent:@"Teste/frases.txt"];
    
    NSString *fileContents = [NSString stringWithContentsOfURL:frasesURL encoding:NSUTF8StringEncoding error:nil];
    
    // CONSTRUIR ARRAY COM CONTEUDO DO FICHEIRO
    NSArray<NSString *> *frases = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    
    char input[255];
    Forca *f1 = [[Forca alloc] initWithFrases:frases];
    
    while ([f1 emJogo]) {
        NSLog(@"Vidas: %lu", (unsigned long)f1.vidas);
        NSLog(@"%@", f1.fraseJogo);
        
        NSLog(@"introduza um caracter:");
        fgets(input, 255, stdin);
        input[strlen(input) -1] = '\0';
        NSString *letra = [NSString stringWithCString:input encoding:NSUTF8StringEncoding];
        
        if ([f1 verificaSeExisteLetra:letra]) {
            NSLog(@"ACERTASTE NA LETRA!!!!");
        } else {
            NSLog(@"AGUA!!!!");
        }
    }
    
    if ([f1 ganhou]) {
        NSLog(@"WEEEEEE....");
    } else {
        NSLog(@"NABO....");
    }

    
    return 0;
}
