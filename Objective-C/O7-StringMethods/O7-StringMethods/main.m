//
//  main.m
//  O7-StringMethods
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSString *s1 = @"Olá malta";
    
    NSLog(@"%lu", (unsigned long)s1.length);
    
    
    for (int i = 0; i < s1.length; i++) {
    
        char c = [s1 characterAtIndex:i];
        
        NSLog(@"%c", c);
    }
    
    return 0;
}
