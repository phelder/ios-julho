//
//  main.m
//  O4-PrimitivasNovas
//
//  Created by Helder Pereira on 07/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    // primitivas de C
    
    int numero;
    long numeroGrande;
    short numeroPequeno;
    
    char caracter;
    
    float numeroDecimal;
    double numeroDecimalGrande;
    
    // primitivas de Objective-C
    // são TypeDef
    
    BOOL boolean; // YES ou NO ... true ou false ... TRUE ou FALSE
    
    NSInteger inteiro = -34;
    NSUInteger inteitoSemSinal = 56;
    
    NSLog(@"___%ld___%lu___", (long)inteiro, (unsigned long)inteitoSemSinal);
    
    return 0;
}
