//
//  main.c
//  C17-StringFunctions
//
//  Created by Helder Pereira on 29/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    
    const char *palavra = "opokemon";
    
    
    bool palindromo = true;
    for (int i = 0; i < strlen(palavra) / 2; i++) {
    
        if (palavra[i] != palavra[strlen(palavra) -1 -i]) {
            palindromo = false;
            break;
        }
    }
    
    if (palindromo) {
        printf("YAY\n");
    } else {
        printf("BOO\n");
    }
    
    printf("\n");
    
    
    char invertida[200];
    
    for (int i = 0; i < strlen(palavra); i++) {
        invertida[i] = palavra[strlen(palavra) -1 -i];
    }
    
    invertida[strlen(palavra)] = '\0';
    
    printf("%s\n", invertida);
    
    if (strcmp(palavra, invertida) == 0) {
        printf("YAY\n");
    } else {
        printf("BOO\n");
    }
    
    
    return 0;
}
