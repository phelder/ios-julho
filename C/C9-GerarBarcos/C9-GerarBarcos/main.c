//
//  main.c
//  C9-GerarBarcos
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define NUM_LINHAS 10
#define NUM_COLS 10
#define NUM_BARCOS 10


int matrizSecreta[NUM_LINHAS][NUM_COLS];


int randomiza(int min, int max) {
    return arc4random() % (max - min + 1) + min;
}

void preencheMatrizSecretaComZeros() {
    for (int i = 0; i < NUM_LINHAS; i++) {
        for (int j = 0; j < NUM_COLS; j++) {
            matrizSecreta[i][j] = 0;
        }
    }
}

void imprimeMatrizSecreta() {
    for (int i = 0; i < NUM_LINHAS; i++) {
        for (int j = 0; j < NUM_COLS; j++) {
            printf(" %d ", matrizSecreta[i][j]);
        }
        printf("\n");
    }
}

bool podeCriarCasa(int linha, int coluna) {
    
    int cima = linha - 1;
    int baixo = linha + 1;
    int esq = coluna - 1;
    int dir = coluna + 1;
    
    if (linha == 0) {
        cima = linha;
    }
    if (linha == NUM_LINHAS - 1) {
        baixo = linha;
    }
    if (coluna == 0) {
        esq = coluna;
    }
    if (coluna == NUM_COLS - 1) {
        dir = coluna;
    }
    
    if (matrizSecreta[linha][coluna] != 0) {
        return false;
    }
    if (matrizSecreta[cima][esq] != 0) {
        return false;
    }
    if (matrizSecreta[cima][coluna] != 0) {
        return false;
    }
    if (matrizSecreta[cima][dir] != 0) {
        return false;
    }
    if (matrizSecreta[linha][dir] != 0) {
        return false;
    }
    if (matrizSecreta[baixo][dir] != 0) {
        return false;
    }
    if (matrizSecreta[baixo][coluna] != 0) {
        return false;
    }
    if (matrizSecreta[baixo][esq] != 0) {
        return false;
    }
    if (matrizSecreta[linha][esq] != 0) {
        return false;
    }
    return true;
}

bool podeCriarBarco(int tamanhoBarco, bool vertical, int linha, int coluna) {
    
    for (int i = 0; i < tamanhoBarco; i++) {
        
        if (vertical) {
            
            if (!podeCriarCasa(linha + i, coluna)) {
                return false;
            }
            
        } else {
            
            if (!podeCriarCasa(linha, coluna + i)) {
                return false;
            }
            
        }
        
    }
    return true;
}

bool criaBarco(int tamanhoBarco) {
    
    int linha;
    int coluna;
    bool vertical;
    
    int contador = 0;
    
    do {
        vertical = randomiza(0, 1);
        
        if (vertical) {
            linha = randomiza(0, NUM_LINHAS - tamanhoBarco);
            coluna = randomiza(0, NUM_COLS - 1);
        } else {
            linha = randomiza(0, NUM_LINHAS - 1);
            coluna = randomiza(0, NUM_COLS - tamanhoBarco);
        }
        
        contador++;
        
        if (contador > 500) {
            return false;
        }
        
    } while(!podeCriarBarco(tamanhoBarco, vertical, linha, coluna));
    
    printf("%d\n", contador);
    
    for (int i = 0; i < tamanhoBarco; i++) {
        if (vertical) {
            matrizSecreta[linha + i][coluna] = tamanhoBarco;
        } else {
            matrizSecreta[linha][coluna + i] = tamanhoBarco;
        }
    }
    
    return true;
}

void preencheMatrizSecretaComBarcos() {
    
    int barcos[NUM_BARCOS] = { 6, 4, 4, 3, 3, 3, 2, 2, 2, 2 };
    
    for (int i = 0; i < NUM_BARCOS; i++) {
        if (!criaBarco(barcos[i])) {
            preencheMatrizSecretaComZeros();
            i = -1;
        }
    }
    
}

int main(int argc, const char * argv[]) {
    
    preencheMatrizSecretaComZeros();
    
    preencheMatrizSecretaComBarcos();
    
    imprimeMatrizSecreta();
    
    return 0;
}