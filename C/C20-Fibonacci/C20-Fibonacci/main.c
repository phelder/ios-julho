//
//  main.c
//  C20-Fibonacci
//
//  Created by Helder Pereira on 01/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, const char * argv[]) {
    
    // INICIAR ARRAY
    int fibo20[20];
    
    
    // PREENCHER COM SEQUENCIA FIBONACCI
    fibo20[0] = 0;
    fibo20[1] = 1;
    for (int i = 2; i < 20; i++) {
        fibo20[i] = fibo20[i-1] + fibo20[i-2];
    }
    
    // IMPRIMIR
    for (int i = 0; i < 20; i++) {
        printf("%d: %d\n", i+1, fibo20[i]);
    }
    
    // AGORA GUARDAR TODOS OS NUMEROS DE FIBONACCI
    // INFERIORES A 1000000
    
    int *fiboN = malloc(2 * sizeof(int));
    
    fiboN[0] = 0;
    fiboN[1] = 1;
    
    int contador = 2;
    
    while (true) {
        
        int num = fiboN[contador - 1] + fiboN[contador - 2];
        
        if (num < 1000000) {
            
            contador++;
            
            fiboN = realloc(fiboN, contador * sizeof(int));
            
            fiboN[contador -1] = num;
        } else {
            break;
        }
        
    }
    
    for (int i = 0; i < contador; i++) {
       printf("%d: %d\n", i+1, fiboN[i]);
    }
    
    free(fiboN);
    
    return 0;
}
