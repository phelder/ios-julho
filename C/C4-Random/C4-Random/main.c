//
//  main.c
//  C4-Random
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int randomiza(int min, int max) {

    //return rand() % (max - min + 1) + min;
    return arc4random() % (max - min + 1) + min;
    
}

int main(int argc, const char * argv[]) {
    
  //  srand((int)time(NULL));
    
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    printf("%d\n", randomiza(1, 10));
    
    return 0;
}
