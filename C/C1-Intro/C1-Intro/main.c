//
//  main.c
//  C1-Intro
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>

int main(int argc, const char * argv[]) {
    
    int a = 12;
    int b = 5;
    
    printf("Olá mundo o valor é %d e o outro valor é %d\n", a, b);
    
    int soma = a + b;
    int subt = a - b;
    int mult = a * b;
    float divi = (float)a / b;
    int rest = a % b;
    
    printf("%d + %d = %d\n", a, b, soma);
    printf("%d - %d = %d\n", a, b, subt);
    printf("%d * %d = %d\n", a, b, mult);
    printf("%d / %d = %.2f\n", a, b, divi);
    printf("%d %% %d = %d\n", a, b, rest);
    
    
    return 0;
}
