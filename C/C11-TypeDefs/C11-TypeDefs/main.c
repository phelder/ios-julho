//
//  main.c
//  C11-TypeDefs
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

typedef int inteirinho;

int main(int argc, const char * argv[]) {
    
    int a = 10;
    inteirinho b = 20;
    
    inteirinho soma = a + b;
    
    printf("%d\n", soma);
    
    return 0;
}
