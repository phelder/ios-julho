//
//  main.c
//  C7-ExercicioMatrizes
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int randomiza(int min, int max) {
    return (arc4random() % (max + 1 - min)) + min;
}

int main(int argc, const char * argv[]) {
    
    // DECLARAR MATRIZ
    int matriz[5][5];
    int somaLinhas[5] = { 0, 0, 0, 0, 0 };
    int somaCols[5] = { 0, 0, 0, 0, 0 };
    
    // PREENCHER MATRIZ
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            matriz[i][j] = randomiza(1, 20);
            somaLinhas[i] += matriz[i][j];
            somaCols[j] += matriz[i][j];
        }
    }
    
    // IMPRIMIR MATRIZ...
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%2d ", matriz[i][j]);
        }
        printf(" = %2d\n",  somaLinhas[i]);
    }
    
    printf(" =  =  =  =  =\n");
    
    for (int i = 0; i < 5; i++) {
        printf("%2d ", somaCols[i]);
    }
    
    printf("\n");
    
    return 0;
}
