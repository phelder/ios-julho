//
//  main.c
//  C16-Strings
//
//  Created by Helder Pereira on 29/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    char string1[10];
    
    string1[0] = 'O';
    string1[1] = 'l';
    string1[2] = 'a';
    string1[3] = ' ';
    string1[4] = 'm';
    string1[5] = 'a';
    string1[6] = 'l';
    string1[7] = 't';
    string1[8] = 'a';
    string1[9] = '\0';
    
    printf("%s\n", string1);
    
    char string2[10] = { 't', 'a', ' ', 't', 'u', 'd', 'o', '?', '?', '\0' };
    
    printf("%s\n", string2);
    
    char string3[10] = "Lala Lala";
    
    string3[0] = 'X';
    
    printf("%s\n", string3);
    
    
    const char *string4 = "buga la";
    
    printf("%s\n", string4);
    
    string4 = "fica ca";
    
    
    printf("%s\n", string4);
    
    
    return 0;
}
