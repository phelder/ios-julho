//
//  main.c
//  C19-AlocacaoMemoria
//
//  Created by Helder Pereira on 01/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    
    int *numeros = malloc(9 * sizeof(int));
    
    numeros[0] = 10;
    numeros[1] = 10;
    numeros[2] = 10;
    numeros[3] = 10;
    numeros[4] = 10;
    numeros[5] = 10;
    numeros[6] = 10;
    numeros[7] = 10;
    numeros[8] = 10;

    numeros = realloc(numeros, 10 * sizeof(int));
    
    numeros[9] = 10;
    
    free(numeros);
    
    numeros = malloc(2 * sizeof(int));
    
    free(numeros);
    
    
    char *nome = malloc(255 * sizeof(char));
    
    printf("Como te chamas?\n");
    
    fgets(nome, 255, stdin);
    nome[strlen(nome) - 1] = '\0';
    
    printf("Olá %s\n", nome);
    
    free(nome);
    
    return 0;
}
