//
//  main.c
//  C14-Apontadores
//
//  Created by Helder Pereira on 29/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int a = 10; // #234
    int b = 15; // #432
    
    printf("APT A: %d\n", &a);
    printf("APT B: %d\n", &b);
    
    int *apt = &a;
    
    printf("APT A: %d\n", apt);
    
    apt = &b;
    
    printf("APT B: %d\n", apt);
    
    
    return 0;
}
