//
//  main.c
//  C5-Input
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int randomiza(int min, int max) {
    return arc4random() % (max - min + 1) + min;
}

int main(int argc, const char * argv[]) {
    
    printf("Introduza um valor, sff\n>");
    
    int numero;
    scanf("%d", &numero);
    
    printf("Obrigado pelo %d e até à próxima\n", numero);
    
    
    int numeroSecreto = randomiza(1, 100);
    int vidas = 7;
    
    do {
    
        printf("Vidas: %d\n", vidas);
        printf("Introduza um número:\n>");
        scanf("%d", &numero);
        
        if (numeroSecreto == numero) {
            break;
        } else {
            if (numeroSecreto > numero) {
                printf("Sobe\n");
            } else {
                printf("Desce\n");
            }
            vidas--;
        }
    } while(vidas > 0);
    
    
    if (vidas > 0) {
        printf("ACERTASTE!!!!!\n");
    } else {
        printf("NABO!!!!!\n");
    }
    
    
    
    return 0;
}