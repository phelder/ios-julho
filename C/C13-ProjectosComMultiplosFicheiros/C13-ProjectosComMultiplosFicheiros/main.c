//
//  main.c
//  C13-ProjectosComMultiplosFicheiros
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include "triangulos.h"

int main(int argc, const char * argv[]) {
    
    Triangulo t1 = criaTriangulo(3, 7);
    
    printf("%f\n", areaTriangulo(t1));
    
    return 0;
}
