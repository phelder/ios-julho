//
//  triangulos.c
//  C13-ProjectosComMultiplosFicheiros
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include "triangulos.h"

Triangulo criaTriangulo(int base, int altura) {

    Triangulo t;
    t.base = base;
    t.altura = altura;
    return t;
}

float areaTriangulo(Triangulo t) {
    
    return t.altura * t.base * 0.5;
    
}