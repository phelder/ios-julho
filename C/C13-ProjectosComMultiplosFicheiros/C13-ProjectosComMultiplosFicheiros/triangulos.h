//
//  triangulos.h
//  C13-ProjectosComMultiplosFicheiros
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#ifndef triangulos_h
#define triangulos_h

#include <stdio.h>

#endif /* triangulos_h */

typedef struct {
    int base;
    int altura;
} Triangulo;

Triangulo criaTriangulo(int base, int altura);

float areaTriangulo(Triangulo t);