//
//  main.c
//  C8-MiniBatalhaNaval
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdbool.h>

int randomiza(int min, int max) {
    return arc4random() % (max - min + 1) + min;
}

void criaMatrizSecreta(int matrizSecreta[10][10]) {
    
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            matrizSecreta[i][j] = 0;
        }
    }
    
    bool vertical = randomiza(0, 1);
    int linha = randomiza(0, 4);
    int coluna = randomiza(0, 4);
    
    matrizSecreta[linha][coluna] = -1;
    
}

void fazBatota(int matrizSecreta[10][10]) {

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            
            printf(" %2d ", matrizSecreta[i][j]);
            
        }
        printf("\n");
    }
}

void resetMatrizJogo(char matrizJogo[10][10]) {
    
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            matrizJogo[i][j] = '.';
        }
    }
    
}

void imprimeMatrizJogo(char matrizJogo[10][10]) {
    
    printf("   ");
    
    for (int i = 0; i < 10; i++) {
        printf(" %d ", i);
    }
    
    printf("\n");
    
    for (int i = 0; i < 10; i++) {
        for (int j = -1; j < 10; j++) {
            if (j == -1) {
                printf(" %d ", i);
            } else {
                printf(" %c ", matrizJogo[i][j]);
            }
        }
        printf("\n");
    }
}


int main(int argc, const char * argv[]) {
    
    int matrizSecreta[10][10] = {
        { 2, 2, 0, 3, 0, 0, 4, 4, 4, 4 },
        { 0, 0, 0, 3, 0, 0, 0, 0, 0, 0 },
        { 4, 0, 0, 3, 0, 3, 3, 3, 0, 2 },
        { 4, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
        { 4, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 4, 0, 0, 0, 0, 2, 0, 0, 0, 3 },
        { 0, 0, 0, 0, 0, 2, 0, 0, 0, 3 },
        { 2, 2, 0, 0, 0, 0, 0, 0, 0, 3 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 6, 6, 6, 6, 6, 6 }
    };

    fazBatota(matrizSecreta);
    
    char matrizJogo[10][10];
    
    resetMatrizJogo(matrizJogo);
    
    int vidas = 30;
    int casasBarcos = 31;
    
    while (vidas > 0) {
        imprimeMatrizJogo(matrizJogo);
        
        printf("Vidas: %d\n", vidas);
        printf("Faltam: %d\n", casasBarcos);
        
        // PEDIR 2 COORDENADAS AO UTILIZADOR
        int linha;
        int coluna;
        
        printf("LINHA:\n>");
        scanf("%d", &linha);
        
        printf("COLUNA:\n>");
        scanf("%d", &coluna);
        
        if (linha < 0 || linha > 9 || coluna < 0 || coluna > 9) {
            printf("-_-\n");
            vidas -= 10;
            continue;
        }
        
        if (matrizSecreta[linha][coluna] == 0) {
            printf("AGUA\n");
            matrizJogo[linha][coluna] = 'x';
            vidas--;
        } else {
            printf("ACERTASTE\n");
            
            if (matrizJogo[linha][coluna] == '.') {
                casasBarcos--;
                if (casasBarcos == 0) {
                    break;
                }
            }
            
            switch (matrizSecreta[linha][coluna]) {
                case 2:
                    matrizJogo[linha][coluna] = 'S';
                    break;
                case 3:
                    matrizJogo[linha][coluna] = 'T';
                    break;
                case 4:
                    matrizJogo[linha][coluna] = 'B';
                    break;
                case 6:
                    matrizJogo[linha][coluna] = 'P';
                    break;
                default:
                    printf("Matriz secreta mal feita....\n");
                    break;
            }
            
//          PARA COLOCAR O CHAR DO NUMERO RESPECTIVO AO TAMANHO DO BARCO
//            matrizJogo[linha][coluna] = matrizSecreta[linha][coluna] + 48;
        }
        
    }
    
    if (vidas > 0) {
        printf("GANHASTE !!!!\n");
    } else {
        printf("NABO !!!!\n");
    }
    
    
    return 0;
}
