//
//  main.c
//  C12-Structs
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>


// CRIAR ESTRUTURA
//struct rectangulo {
//    int ladoA;
//    int ladoB;
//};


// CRIAR ESTRUTURA COM TYPEDEF
//typedef struct rectangulo {
//    int ladoA;
//    int ladoB;
//} Rect;

// FORMA FIXE: STRUCt ANONIMA COM tYPEDEF
typedef struct {
    int ladoA;
    int ladoB;
} Rect;

int perimetro(Rect r) {
    return (r.ladoA + r.ladoB) * 2;
}

int area(Rect r) {
    return r.ladoA * r.ladoB;
}

int main(int argc, const char * argv[]) {
    
    Rect r1;
    
    r1.ladoA = 10;
    r1.ladoB = 5;
    
    Rect r2;
    
    r2.ladoA = 8;
    r2.ladoB = 9;
    
    if (area(r1) > area(r2)) {
        printf("R1\n");
    } else {
        printf("NAO R1\n");
    }
    
    return 0;
}
