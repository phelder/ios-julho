//
//  main.c
//  C15-PassagensPorValorOuReferencia
//
//  Created by Helder Pereira on 29/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>

void testePassagemValor(int numero) {
    
    numero++;
    
}

void testePassagemReferencia(int *apt) {
    
    (*apt)++;
    
    //apt[0]++; // Giro...
    
}

void testeArray(int array[]) {

    printf("SIZE OF ArRAY: %lu\n", sizeof(array));
    
    array[2] = 1;

}


int main(int argc, const char * argv[]) {
    
    int a = 10;
    
//    testePassagemValor(a);
    
    testePassagemReferencia(&a);
    
    printf("%d\n", a);
//
//    int numeros[] = { 10, 20, 5, 15, 40, 32, 64 };
//    
//    testeArray(numeros);
//    
//    for (int i = 0; i < 7; i++) {
//        printf("%d: %d\n", i, numeros[i]);
//    }
//    
//    
//    int sizeOfInt = sizeof(numeros) / sizeof(int);
//    
//    printf("%d\n", sizeOfInt);
    
    return 0;
}
