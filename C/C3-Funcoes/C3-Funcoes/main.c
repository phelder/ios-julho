//
//  main.c
//  C3-Funcoes
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

int soma(int n1, int n2) {
    
    return n1 + n2;
    
}

bool primo(int n) {
    
    for (int i = 2; i <= sqrt(n); i++) {
        
        if (n % i == 0) {
            return false;
        }
        
    }
    return true;
}

bool jaExiste(int agulha, int palheiroC, int palheiro[]) {
    
    for (int i = 0; i < palheiroC; i++) {
        if (agulha == palheiro[i]) {
            return true;
        }
    }
    return false;
}


int main(int argc, const char * argv[]) {
    
    for (int i = 0; i < 10; i++) {
        if (primo(i)) {
            printf("%d\n", i);
        }
    }
    
    
    
    int numeros[10] = { 1, 4, 6, 8, 12, 34, 12, 45, 60, 73 };
    
    if (jaExiste(1, 10, numeros)) {
        printf("JA EXISTE\n");
    } else {
        printf("NAO EXISTE\n");
    }
    

    
    return 0;
}