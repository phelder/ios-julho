//
//  main.c
//  C6-Matrizes
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int matriz1[5][5] = {
        { 1, 2, 3, 4, 5 },
        { 10, 20, 30, 40, 50 },
        { 6, 7, 8, 9, 10 },
        { 11, 12, 13, 14, 15 },
        { 21, 22, 23, 24, 25 }
    };
    
    
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            
            printf("%3d ", matriz1[i][j]);
            
        }
        printf("\n");
    }
    
    
    return 0;
}
