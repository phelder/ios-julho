//
//  main.c
//  C2-Arrays
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int numeros[10];
    
    numeros[0] = 5;
    numeros[1] = 35;
    numeros[2] = 35;
    numeros[3] = 56;
    numeros[4] = 2;
    numeros[5] = 65;
    numeros[6] = 12;
    numeros[7] = 54;
    numeros[8] = 65;
    numeros[9] = 56;
    
    int maior = numeros[0];
    int contador = 1;
    
    for (int i = 1; i < 10; i++) {
        printf("%d: %d\n", i, numeros[i]);
        
        if (numeros[i] > maior) {
            maior = numeros[i];
            contador = 1;
        } else if(numeros[i] == maior) {
            contador++;
        }
    }
    
    printf("MAIOR: %d\n", maior);
    printf("Aparece %dX\n", contador);
    
    
    
    return 0;
}
