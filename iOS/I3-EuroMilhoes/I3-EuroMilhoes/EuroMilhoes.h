//
//  EuroMilhoes2.h
//  O18-EuroMilhoes
//
//  Created by Helder Pereira on 16/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EuroMilhoes : NSObject

@property (strong, nonatomic, readonly) NSArray<NSNumber *> *numeros;
@property (strong, nonatomic, readonly) NSArray<NSNumber *> *estrelas;

@property (strong, nonatomic, readonly) NSString *numerosString;
@property (strong, nonatomic, readonly) NSString *estrelasString;

@property (strong, nonatomic, readonly) NSString *chave;

- (NSComparisonResult)compare:(EuroMilhoes *)otherObject;

@end
