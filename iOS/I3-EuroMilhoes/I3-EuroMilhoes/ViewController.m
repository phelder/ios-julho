//
//  ViewController.m
//  I3-EuroMilhoes
//
//  Created by Helder Pereira on 21/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "EuroMilhoes.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (IBAction)clickedGerarChaves:(id)sender {
    
    for (int i = 0; i < self.labels.count; i++) {
        EuroMilhoes *em = [[EuroMilhoes alloc] init];
        
        UILabel *label = self.labels[i];
        
        label.text = [NSString stringWithFormat:@"Chave %d: %@", i+1, em];
    }
    
}

@end
