//
//  Song.m
//  I21-SongBase
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Song.h"

@implementation Song

- (instancetype)initWithWebId:(NSUInteger)webId title:(NSString *)title artist:(NSString *)artist duration:(NSString *)duration thumbURL:(NSString *)thumbURL lyrics:(NSString *)lyrics
{
    self = [super init];
    if (self) {
        _webId = webId;
        _title = title;
        _artist = artist;
        _duration = duration;
        _thumbURL = thumbURL;
        _lyrics = lyrics;
    }
    return self;
}

+ (instancetype)songWithWebId:(NSUInteger)webId title:(NSString *)title artist:(NSString *)artist duration:(NSString *)duration thumbURL:(NSString *)thumbURL lyrics:(NSString *)lyrics
{
    return [[Song alloc] initWithWebId:webId title:title artist:artist duration:duration thumbURL:thumbURL lyrics:lyrics];
}

@end
