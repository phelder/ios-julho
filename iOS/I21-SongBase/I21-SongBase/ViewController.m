//
//  ViewController.m
//  I21-SongBase
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Song.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewMusic;

@end

@implementation ViewController
{
    NSArray <Song *> *_music;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // DESCARREGAR INFO DA NET...
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
// JA E JSON POR DEFEITO...
//  manager.responseSerializer = [[AFJSONResponseSerializer alloc] init];
    
//    NSDictionary *params = @{
//                             @"tipo": @"destaques"
//                             };
    
    NSDictionary *params = @{
                             @"tipo": @"buscaartista",
                             @"nome": @"mi"
                             };
    
    // CRIAR REQUEST
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:@"http://reality6.com/musicservicejson.php" parameters:nil error:nil];

    // ENVIAR REQUEST E RECEBER RESPONSE
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
    
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            //NSLog(@"%@ %@", response, responseObject);
            
            [self tratarDados:responseObject];
        }
    }];
    [dataTask resume];
    
}

- (void)tratarDados:(NSArray *)responseObject {

    NSMutableArray<Song *> *songs = [[NSMutableArray alloc] init];
    
    for (NSDictionary<NSString *, NSString *> *object in responseObject) {
        
        Song *s = [[Song alloc] init];
        s.webId = object[@"id"].integerValue;
        s.title = object[@"title"];
        s.artist = object[@"artist"];
        s.duration = object[@"duration"];
        s.thumbURL = object[@"thumb_url"];
        s.lyrics = object[@"lyrics"];
        
        [songs addObject:s];
    }
    
    _music = [NSArray arrayWithArray:songs];
    
    [self.tableViewMusic reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _music.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    Song *s = _music[indexPath.row];
    
    cell.textLabel.text = s.title;
    cell.detailTextLabel.text = s.artist;
    
    NSURL *downloadURL = [NSURL URLWithString:s.thumbURL];
    UIImage *placeholderImage = [UIImage imageNamed:@"ThumbLoading"];
    
    [cell.imageView setImageWithURL:downloadURL placeholderImage:placeholderImage];
    
    return cell;
}


@end
