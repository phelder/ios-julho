//
//  Song.h
//  I21-SongBase
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Song : NSObject

@property (assign, nonatomic) NSUInteger webId;
@property (assign, nonatomic) NSString *title;
@property (assign, nonatomic) NSString *artist;
@property (assign, nonatomic) NSString *duration;
@property (assign, nonatomic) NSString *thumbURL;
@property (assign, nonatomic) NSString *lyrics;

- (instancetype) initWithWebId:(NSUInteger)webId title:(NSString *)title artist:(NSString *)artist duration:(NSString *)duration thumbURL:(NSString *)thumbURL lyrics:(NSString *)lyrics;

+ (instancetype) songWithWebId:(NSUInteger)webId title:(NSString *)title artist:(NSString *)artist duration:(NSString *)duration thumbURL:(NSString *)thumbURL lyrics:(NSString *)lyrics;


@end
