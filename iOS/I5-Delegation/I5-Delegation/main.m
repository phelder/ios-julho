//
//  main.m
//  I5-Delegation
//
//  Created by Helder Pereira on 23/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
