//
//  ViewController.m
//  I5-Delegation
//
//  Created by Helder Pereira on 23/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIAlertViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)clickedShowAlert:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] init];
    
    alertView.title = @"Titulo";
    alertView.message = @"Uma mensagem qualquer";
    
    [alertView addButtonWithTitle:@"OK"];
    [alertView addButtonWithTitle:@"OH NO..."];
    [alertView addButtonWithTitle:@"Maybe"];
    
    alertView.cancelButtonIndex = 1;
    
    alertView.delegate = self;
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    NSLog(@"%ld", (long)buttonIndex);

}

- (void)willPresentAlertView:(UIAlertView *)alertView {
    
    NSLog(@"AI VEM ELA");
    
}

- (void)didPresentAlertView:(UIAlertView *)alertView {

    NSLog(@"JA CHEGOU");
}

@end
