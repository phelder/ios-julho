//
//  ViewController.m
//  I9-AutoLayout
//
//  Created by Helder Pereira on 26/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

#pragma mark - UIButton Actions

- (IBAction)clickedButton:(id)sender {
    
    self.label1.text = [self.label1.text stringByAppendingString:@"OLA "];
    
}



@end
