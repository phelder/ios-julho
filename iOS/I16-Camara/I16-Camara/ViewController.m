//
//  ViewController.m
//  I16-Camara
//
//  Created by Helder Pereira on 04/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIButton *buttonTakePicture;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.buttonTakePicture.enabled = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];

}

#pragma mark - UIButton Actions

- (IBAction)clickedButtonTakePicture:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    }
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

- (IBAction)clickedButtonChoosePicture:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)clickedButtonSave:(id)sender {
    
    NSData *imageData = UIImagePNGRepresentation(self.imageView1.image);

// BRINCADEIRA...
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:imageData forKey:@"img1"];
//    
//    [defaults synchronize];
    
// AGORA A SERIO...
    
    NSURL *docURL = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *fileURL = [docURL URLByAppendingPathComponent:@"img1.png"];
    
    [imageData writeToURL:fileURL atomically:YES];
}

- (IBAction)clickedButtonLoad:(id)sender {
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *imageData = [defaults objectForKey:@"img1"];
//    
//    self.imageView1.image = [UIImage imageWithData:imageData];
    
    NSURL *docURL = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *fileURL = [docURL URLByAppendingPathComponent:@"img1.png"];
    
    self.imageView1.image = [UIImage imageWithContentsOfFile:fileURL.path];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.imageView1.image = info[UIImagePickerControllerEditedImage];
    
    NSLog(@"%@", info);
    
}


@end
