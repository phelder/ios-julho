//
//  SecondViewController.m
//  I12-PadraoSingleton
//
//  Created by Helder Pereira on 30/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"
#import "MySingleton.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    MySingleton *mySingleton = [MySingleton sharedInstance];

    
    NSLog(@"%@", mySingleton.dados);
}

@end
