//
//  MySingleton.h
//  I12-PadraoSingleton
//
//  Created by Helder Pereira on 30/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySingleton : NSObject

@property (strong, nonatomic) NSString *valor1;
@property (strong, nonatomic) NSMutableArray *dados;

+ (instancetype)sharedInstance;

@end
