//
//  MySingleton.m
//  I12-PadraoSingleton
//
//  Created by Helder Pereira on 30/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MySingleton.h"

@implementation MySingleton

+ (instancetype)sharedInstance {

//    static MySingleton *instance = nil;
//    
//    @synchronized (self) { // THREAD SAFETY... OLD WAY
//        if (instance == nil) {
//            instance = [[MySingleton alloc] init];
//        }
//    }
//    
//    return instance;
    
    static MySingleton *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance = [[MySingleton alloc] init];
        
        instance.dados = [[NSMutableArray alloc] init];
        
    });
    
    return instance;
    
}

@end
