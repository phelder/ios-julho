//
//  ViewController.m
//  I12-PadraoSingleton
//
//  Created by Helder Pereira on 30/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "MySingleton.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)clickedButton1:(id)sender {
    
    static int i = 0;
    i++;
    
    NSString *title = [NSString stringWithFormat:@"%d", i];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)clickedButton2:(id)sender {

    MySingleton *mySingleton = [MySingleton sharedInstance];
    
    [mySingleton.dados addObject:@"A"];
    [mySingleton.dados addObject:@"B"];
    [mySingleton.dados addObject:@"C"];
    [mySingleton.dados addObject:@"D"];
    [mySingleton.dados addObject:@"E"];
    [mySingleton.dados addObject:@"F"];
    [mySingleton.dados addObject:@"G"];
    [mySingleton.dados addObject:@"H"];
    [mySingleton.dados addObject:@"I"];
    
}

@end
