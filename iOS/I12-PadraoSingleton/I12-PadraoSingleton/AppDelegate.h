//
//  AppDelegate.h
//  I12-PadraoSingleton
//
//  Created by Helder Pereira on 30/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

