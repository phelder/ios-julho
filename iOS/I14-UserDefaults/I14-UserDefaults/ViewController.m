//
//  ViewController.m
//  I14-UserDefaults
//
//  Created by Helder Pereira on 03/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textFieldTexto;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.textFieldTexto.text = [defaults objectForKey:@"texto1"];
    
}

- (IBAction)clickedButtonGuardar:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:self.textFieldTexto.text forKey:@"texto1"];
    
    [defaults synchronize];
    
}



@end
