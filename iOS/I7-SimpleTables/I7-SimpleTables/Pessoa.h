//
//  Pessoa.h
//  O15-Compare
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (assign, nonatomic) NSUInteger idade;
@property (strong, nonatomic) NSString *cidade;

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade;

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade;

- (NSComparisonResult)compareByNome:(Pessoa *)outraPessoa;
- (NSComparisonResult)compareByIdade:(Pessoa *)outraPessoa;
- (NSComparisonResult)compareByCidade:(Pessoa *)outraPessoa;

@end
