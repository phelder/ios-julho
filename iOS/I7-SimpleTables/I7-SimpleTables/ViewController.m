//
//  ViewController.m
//  I7-SimpleTables
//
//  Created by Helder Pereira on 23/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Pessoa.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController
{
    NSArray<Pessoa *> *_gente;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _gente = @[
               [Pessoa pessoaWithNome:@"André" idade:10 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Xana" idade:12 cidade:@"Lisboa"],
               [Pessoa pessoaWithNome:@"Tiago" idade:30 cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Gigi" idade:32 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Emanuel" idade:40 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Lopes" idade:41 cidade:@"Aveiro"],
               [Pessoa pessoaWithNome:@"Manel" idade:3 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"NO" idade:9 cidade:@"Lisboa"]
               ];

}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _gente.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    Pessoa *p = _gente[indexPath.row];
    
    cell.textLabel.text = p.nome;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %lu anos", p.cidade, (unsigned long)p.idade];
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Pessoa *p = _gente[indexPath.row];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:p.description message:nil preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSLog(@"BOTAO YAY");
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel..." style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        NSLog(@"BOTAO YAY");
        
    }];
    
    UIAlertAction *destroyAction = [UIAlertAction actionWithTitle:@"DESTROY!!!!" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        NSLog(@"BOTAO YAY");
        
    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    [alert addAction:destroyAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
