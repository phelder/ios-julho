//
//  AppDelegate.h
//  I8-CustomTables
//
//  Created by Helder Pereira on 26/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

