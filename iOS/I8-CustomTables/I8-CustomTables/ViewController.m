//
//  ViewController.m
//  I8-CustomTables
//
//  Created by Helder Pereira on 26/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Pessoa.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController
{
    NSArray<Pessoa *> *_gente;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _gente = @[
               [Pessoa pessoaWithNome:@"André" idade:10 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Xana" idade:12 cidade:@"Lisboa"],
               [Pessoa pessoaWithNome:@"Tiago" idade:30 cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Gigi" idade:32 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Emanuel" idade:40 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Lopes" idade:41 cidade:@"Aveiro"],
               [Pessoa pessoaWithNome:@"Manel" idade:3 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"NO" idade:9 cidade:@"Lisboa"]
               ];
    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _gente.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    UILabel *labeNome = [cell viewWithTag:1];
    UILabel *labeCidade = [cell viewWithTag:2];
    UILabel *labeIdade = [cell viewWithTag:3];
    
    Pessoa *pessoa = _gente[indexPath.row];
    
    labeNome.text = pessoa.nome;
    labeCidade.text = pessoa.cidade;
    labeIdade.text = [NSString stringWithFormat:@"%lu anos", (unsigned long)pessoa.idade];
    
    return cell;
}


@end
