//
//  EditViewController.m
//  I13-ListaAmigos
//
//  Created by Helder Pereira on 03/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "EditViewController.h"
#import "PessoasDataSource.h"

@interface EditViewController ()

@property (weak, nonatomic) IBOutlet UITextField *labelNome;
@property (weak, nonatomic) IBOutlet UITextField *labelIdade;
@property (weak, nonatomic) IBOutlet UITextField *labelCidade;

@end

@implementation EditViewController
{
    BOOL _novoAmigo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.amigo) {
        
        self.navigationItem.title = self.amigo.nome;
        
        self.labelNome.text = self.amigo.nome;
        self.labelIdade.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.amigo.idade];
        self.labelCidade.text = self.amigo.cidade;
    } else {
        _novoAmigo = YES;
    }
}

#pragma mark - UIButton Actions

- (IBAction)clickedGuardar:(id)sender {
    
    if (_novoAmigo) {
        self.amigo = [[Pessoa alloc] init];
    }
    
    self.amigo.nome = self.labelNome.text;
    self.amigo.idade = self.labelIdade.text.integerValue;
    self.amigo.cidade = self.labelCidade.text;
    
    if (_novoAmigo) {
        PessoasDataSource *pds = [PessoasDataSource sharedDataSource];
        [pds.amigos addObject:self.amigo];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
