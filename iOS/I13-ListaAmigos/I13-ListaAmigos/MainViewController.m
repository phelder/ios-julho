//
//  MainViewController.m
//  I13-ListaAmigos
//
//  Created by Helder Pereira on 03/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "PessoasDataSource.h"
#import "EditViewController.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewAmigos;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // preenchimento dummy para testes...
    PessoasDataSource *pds = [PessoasDataSource sharedDataSource];
    [pds.amigos addObject:[Pessoa pessoaWithNome:@"A" idade:10 cidade:@"P"]];
    [pds.amigos addObject:[Pessoa pessoaWithNome:@"B" idade:9 cidade:@"P"]];
    [pds.amigos addObject:[Pessoa pessoaWithNome:@"C" idade:12 cidade:@"L"]];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.tableViewAmigos reloadData];
    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    PessoasDataSource *pds = [PessoasDataSource sharedDataSource];
    
    return pds.amigos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    PessoasDataSource *pds = [PessoasDataSource sharedDataSource];
    
    Pessoa *p = pds.amigos[indexPath.row];
    
    cell.textLabel.text = p.nome;
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PessoasDataSource *pds = [PessoasDataSource sharedDataSource];
    
    Pessoa *p = pds.amigos[indexPath.row];
    
    
    [self performSegueWithIdentifier:@"MainToEdit" sender:p];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PessoasDataSource *pds = [PessoasDataSource sharedDataSource];
    [pds.amigos removeObjectAtIndex:indexPath.row];
    
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UIButton Actions

- (IBAction)clickedAdicionar:(id)sender {
    
    [self performSegueWithIdentifier:@"MainToEdit" sender:nil];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"MainToEdit"]) {
        if ([sender isKindOfClass:[Pessoa class]]) {
            EditViewController *evc = segue.destinationViewController;
            evc.amigo = sender;
        }
    }
}


@end
