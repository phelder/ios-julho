//
//  Pessoa.m
//  I13-ListaAmigos
//
//  Created by Helder Pereira on 03/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
}

@end
