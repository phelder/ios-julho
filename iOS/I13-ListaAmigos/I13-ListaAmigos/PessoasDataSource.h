//
//  PessoasDataSource.h
//  I13-ListaAmigos
//
//  Created by Helder Pereira on 03/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

@interface PessoasDataSource : NSObject

@property (strong, nonatomic) NSMutableArray<Pessoa *> *amigos;

+ (instancetype)sharedDataSource;

@end
