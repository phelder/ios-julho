//
//  PessoasDataSource.m
//  I13-ListaAmigos
//
//  Created by Helder Pereira on 03/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "PessoasDataSource.h"


@implementation PessoasDataSource

+ (instancetype)sharedDataSource {

    static PessoasDataSource *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{

        instance = [[PessoasDataSource alloc] init];
        instance.amigos = [[NSMutableArray alloc] init];
        
    });
    
    return instance;
}

@end
