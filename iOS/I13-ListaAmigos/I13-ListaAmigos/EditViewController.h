//
//  EditViewController.h
//  I13-ListaAmigos
//
//  Created by Helder Pereira on 03/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Pessoa;

@interface EditViewController : UIViewController

@property (strong, nonatomic) Pessoa *amigo;

@end
