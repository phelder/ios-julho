//
//  PhotoTableViewController.m
//  I17-PhotoTable
//
//  Created by Helder Pereira on 07/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "PhotoTableViewController.h"
#import "AppDelegate.h"
#import "PhotoBigViewController.h"

@interface PhotoTableViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonCamera;

@end

@implementation PhotoTableViewController
{
    NSArray<NSURL *> *_photoURLs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        self.barButtonCamera.enabled = NO;
        
    }
    
    [self fillArrayPhotos];
    [self.tableView reloadData];
 
}

#pragma mark - UIBarButton Actions

- (IBAction)clickedGallery:(id)sender {
  
    [self goPickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (IBAction)clickedCamera:(id)sender {
    
    [self goPickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
}

- (void)goPickerWithSourceType:(UIImagePickerControllerSourceType)source {
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.sourceType = source;
    
    picker.allowsEditing = YES;
    
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {

    [self dismissViewControllerAnimated:YES completion:nil];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSURL *imagesURL = appDelegate.imagesFolder;

    
    UIImage *pickedImage = info[UIImagePickerControllerEditedImage];
    NSData *imageData = UIImagePNGRepresentation(pickedImage);
    
    
    NSString *newFileName = [NSString stringWithFormat:@"%@.png", [NSDate date]];
    NSURL *saveURL = [imagesURL URLByAppendingPathComponent:newFileName];
    
    
    [imageData writeToURL:saveURL atomically:YES];
    
    [self fillArrayPhotos];
    [self.tableView reloadData];
}

- (void)fillArrayPhotos {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSURL *imagesURL = appDelegate.imagesFolder;
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    _photoURLs = [fileManager contentsOfDirectoryAtURL:imagesURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];

}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _photoURLs.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath];
    
    UIImage *photo = [UIImage imageWithContentsOfFile:_photoURLs[indexPath.row].path];
    
    UIImageView *cellImageView = [cell viewWithTag:1];
    
    cellImageView.image = photo;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIImage *photo = [UIImage imageWithContentsOfFile:_photoURLs[indexPath.row].path];
    
    [self performSegueWithIdentifier:@"PhotoTableToPhotoBig" sender:photo];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:_photoURLs[indexPath.row] error:nil];
    
    [self fillArrayPhotos];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"PhotoTableToPhotoBig"]) {
        
        if ([sender isKindOfClass:[UIImage class]]) {
        
            PhotoBigViewController *pbvc = segue.destinationViewController;
            pbvc.photo = sender;
            
        }
    }
    
}


@end
