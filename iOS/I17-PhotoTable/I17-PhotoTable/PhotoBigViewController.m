//
//  PhotoBigViewController.m
//  I17-PhotoTable
//
//  Created by Helder Pereira on 07/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "PhotoBigViewController.h"

@interface PhotoBigViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhoto;

@end

@implementation PhotoBigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.imageViewPhoto.image = self.photo;
    
}

@end
