//
//  UIColor+MyColors.m
//  I2-Contadores
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "UIColor+MyColors.h"

@implementation UIColor (MyColors)

+ (instancetype)myRedColor {
    return [UIColor colorWithRed:1 green:0.4 blue:0.5 alpha:1];
}

+ (instancetype)myGreenColor {
    return [UIColor colorWithRed:0.8 green:1 blue:0.3 alpha:1];
}

@end
