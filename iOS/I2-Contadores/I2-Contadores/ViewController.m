//
//  ViewController.m
//  I2-Contadores
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "UIColor+MyColors.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel* labelContador1;
@property (weak, nonatomic) IBOutlet UILabel* labelContador2;
@property (weak, nonatomic) IBOutlet UISegmentedControl* segmentContadores;

@end

@implementation ViewController
{
    int _contador1;
    int _contador2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self actualizaContadores];
}

- (void)actualizaContadores {
    self.labelContador1.text = [NSString stringWithFormat:@"Contador 1: %d", _contador1];
    
    self.labelContador2.text = [NSString stringWithFormat:@"Contador 2: %d", _contador2];

    if (_contador1 > _contador2) {
        self.view.backgroundColor = [UIColor myGreenColor];
    } else if (_contador1 < _contador2) {
        self.view.backgroundColor = [UIColor myRedColor];
    } else {
        self.view.backgroundColor = [UIColor whiteColor];
    }
}

- (IBAction)clickedSobe:(id)sender {

    if (self.segmentContadores.selectedSegmentIndex == 0) {
        _contador1++;
    } else {
        _contador2++;
    }
    [self actualizaContadores];
}

- (IBAction)clickedDesce:(id)sender {
    
    if (self.segmentContadores.selectedSegmentIndex == 0) {
        _contador1--;
    } else {
        _contador2--;
    }
    [self actualizaContadores];
}



@end
