//
//  UIColor+MyColors.h
//  I2-Contadores
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MyColors)

+ (instancetype)myRedColor;
+ (instancetype)myGreenColor;

@end
