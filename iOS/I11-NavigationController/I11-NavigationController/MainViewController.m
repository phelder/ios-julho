//
//  MainViewController.m
//  I11-NavigationController
//
//  Created by Helder Pereira on 28/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "Pessoa.h"
#import "DetailViewController.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation MainViewController
{
    NSArray<Pessoa *> *_gente;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _gente = @[
               [Pessoa pessoaWithNome:@"André" idade:10 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Xana" idade:12 cidade:@"Lisboa"],
               [Pessoa pessoaWithNome:@"Tiago" idade:30 cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Gigi" idade:32 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Emanuel" idade:40 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Lopes" idade:41 cidade:@"Aveiro"],
               [Pessoa pessoaWithNome:@"Manel" idade:3 cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"NO" idade:9 cidade:@"Lisboa"]
               ];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _gente.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    Pessoa *pessoa = _gente[indexPath.row];
    
    cell.textLabel.text = pessoa.nome;
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Pessoa *p = _gente[indexPath.row];
    
    [self performSegueWithIdentifier:@"MainToDetail" sender:p];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"MainToDetail"]) {
        
        DetailViewController *dvc = segue.destinationViewController;
        
        dvc.pessoa = sender;
    }
    
}


@end
