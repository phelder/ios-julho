//
//  DetailViewController.m
//  I11-NavigationController
//
//  Created by Helder Pereira on 28/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "Pessoa.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelNome;
@property (weak, nonatomic) IBOutlet UILabel *labelIdade;
@property (weak, nonatomic) IBOutlet UILabel *labelCidade;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.pessoa.nome;

    self.labelNome.text = self.pessoa.nome;
    self.labelIdade.text = [NSString stringWithFormat:@"%lu anos", (unsigned long)self.pessoa.idade];
    self.labelCidade.text = self.pessoa.cidade;
    
//    [self.navigationController popViewControllerAnimated:YES];
}


@end
