//
//  DetailViewController.h
//  I11-NavigationController
//
//  Created by Helder Pereira on 28/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Pessoa;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Pessoa *pessoa;

@end
