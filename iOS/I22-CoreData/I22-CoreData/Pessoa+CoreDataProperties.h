//
//  Pessoa+CoreDataProperties.h
//  I22-CoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Pessoa (CoreDataProperties)

+ (NSFetchRequest<Pessoa *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *nome;
@property (nonatomic) int32_t idade;
@property (nullable, nonatomic, copy) NSString *cidade;

@end

NS_ASSUME_NONNULL_END
