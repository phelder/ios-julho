//
//  ViewController.m
//  I22-CoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "Pessoa+CoreDataClass.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self insertPerson];
    [self loadPeople];
}

- (void)loadPeople {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
  //  NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Pessoa"];

    NSFetchRequest *pessoaRequest = [Pessoa fetchRequest];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nome CONTAINS [cd]%@", @"T"];
    
    NSSortDescriptor *sortByNome = [NSSortDescriptor sortDescriptorWithKey:@"nome" ascending:YES];
    
    [pessoaRequest setPredicate:predicate];
    [pessoaRequest setSortDescriptors:@[sortByNome]];
    
    NSArray<Pessoa *> *gente = [context executeFetchRequest:pessoaRequest error:nil];
    
    
    
    NSLog(@"%@", gente);
}

- (void)insertPerson {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    Pessoa *newPessoa = [NSEntityDescription insertNewObjectForEntityForName:@"Pessoa" inManagedObjectContext:context];
    
    newPessoa.nome = @"Tobias";
    newPessoa.idade = 5;
    newPessoa.cidade = @"Lisboa";
    
    [appDelegate saveContext];
    
}


@end
