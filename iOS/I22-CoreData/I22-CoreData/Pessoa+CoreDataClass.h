//
//  Pessoa+CoreDataClass.h
//  I22-CoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Pessoa : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Pessoa+CoreDataProperties.h"
