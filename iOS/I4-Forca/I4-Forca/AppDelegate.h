//
//  AppDelegate.h
//  I4-Forca
//
//  Created by Helder Pereira on 21/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

