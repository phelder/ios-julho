//
//  Forca.m
//  O13-Forca
//
//  Created by Developer on 9/14/16.
//  Copyright © 2016 Flag. All rights reserved.
//

#import "Forca.h"

@implementation Forca
{
    NSString *_fraseSecreta;
    NSMutableString *_fraseJogo;
    NSUInteger _vidas;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self iniciaJogoComFrase:@"frase por defeito"];
    }
    return self;
}

- (instancetype)initWithFrase:(NSString *)frase
{
    self = [super init];
    if (self) {
        [self iniciaJogoComFrase:frase];
    }
    return self;
}

- (instancetype)initWithFrases:(NSArray<NSString *> *)frases {
    self = [super init];
    if (self) {
        int pos = arc4random() % frases.count;
        [self iniciaJogoComFrase:frases[pos]];
    }
    return self;
}

- (void)iniciaJogoComFrase:(NSString *)frase {

    _vidas = 6;
    
    _fraseSecreta = frase;
    _fraseJogo = [[NSMutableString alloc] init];
    
    for (int i = 0; i < _fraseSecreta.length; i++) {
        
        unichar letra = [_fraseSecreta characterAtIndex:i];
        
        if (letra == ' ') {
            [_fraseJogo appendString:@" "];
        } else {
            [_fraseJogo appendString:@"-"];
        }
    }
}

- (NSString *)fraseJogo {
    return [NSString stringWithString:_fraseJogo];
}

- (NSUInteger)vidas {
    return _vidas;
}

- (BOOL)verificaSeExisteLetra:(NSString *)letra {
    
    BOOL existe = NO;
    
    for (int i = 0; i < _fraseSecreta.length; i++) {
        
        NSRange range = NSMakeRange(i, 1);
        NSString *letraFrase = [_fraseSecreta substringWithRange:range];
        
        if ([letraFrase compare:letra options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch] == NSOrderedSame) {
        
            [_fraseJogo replaceCharactersInRange:range withString:letraFrase];
            
            existe = YES;
            
        }
    
    }
    
    if (!existe) {
        _vidas--;
    }
    
    return existe;
}

- (BOOL)emJogo {

    if (_vidas > 0 && ![_fraseSecreta isEqualToString:_fraseJogo]) {
        return YES;
    }
    return NO;
}

- (BOOL)ganhou {
    
    if (![self emJogo] && _vidas > 0) {
        return YES;
    }
    return NO;

}

@end
