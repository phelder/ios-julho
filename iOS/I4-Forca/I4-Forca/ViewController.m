//
//  ViewController.m
//  I4-Forca
//
//  Created by Helder Pereira on 21/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Forca.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageViewForca;
@property (weak, nonatomic) IBOutlet UILabel *labelVidas;
@property (weak, nonatomic) IBOutlet UILabel *labelFrase;
@property (weak, nonatomic) IBOutlet UILabel *labelEstado;

@end

@implementation ViewController
{
    Forca *_jogoForca;
    NSArray <UIImage *> *_images;
    NSMutableArray <UIButton *> *_disabledButtons;
    int _contadorExtra;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _images = @[
                [UIImage imageNamed:@"Forca0"],
                [UIImage imageNamed:@"Forca1"],
                [UIImage imageNamed:@"Forca2"],
                [UIImage imageNamed:@"Forca3"],
                [UIImage imageNamed:@"Forca4"],
                [UIImage imageNamed:@"Forca5"],
                [UIImage imageNamed:@"Forca6"]
                ];
    
    [self novoJogo];
}

- (void)actualizaEcra {
    
    self.labelVidas.text = [NSString stringWithFormat:@"%lu", (unsigned long)_jogoForca.vidas];
    self.labelFrase.text = _jogoForca.fraseJogo;
    
//    NSString *imageName = [NSString stringWithFormat:@"Forca%lu", (unsigned long)_jogoForca.vidas];
//    self.imageViewForca.image = [UIImage imageNamed:imageName];
    
    self.imageViewForca.image = _images[_jogoForca.vidas];
}

- (void)novoJogo {
    
    _jogoForca = [[Forca alloc] initWithFrase:@"ABC"];
    
    self.labelEstado.text = nil;
    
    [self actualizaEcra];
    
    for (UIButton *button in _disabledButtons) {
        button.enabled = YES;
    }
    _disabledButtons = [[NSMutableArray alloc] init];
    _contadorExtra = 0;
}

- (IBAction)clickedLetter:(id)sender {
    
    if (!_jogoForca.emJogo) {
        
        _contadorExtra++;
        
        if (_contadorExtra == 1) {
            self.labelEstado.text = @"já acabou...";
        }
        if (_contadorExtra == 2) {
            self.labelEstado.text = @"a sério ...";
        }
        if (_contadorExtra == 3) {
            self.labelEstado.text = @"faz novo jogo moço ...";
        }
        if (_contadorExtra == 4) {
            self.labelEstado.text = @"-_-";
        }
        
        return;
    }
    
    UIButton *letterButton = sender;
    NSString *letter = letterButton.currentTitle;
    
    letterButton.enabled = NO;
    [_disabledButtons addObject:letterButton];
    
    [_jogoForca verificaSeExisteLetra:letter];
    
    [self actualizaEcra];
    
    if (!_jogoForca.emJogo) {
        if (_jogoForca.ganhou) {
            self.labelEstado.text = @"GANHASTE";
        } else {
            self.labelEstado.text = @"PERDESTE";
        }
    }
}

- (IBAction)clickedNovoJogo:(id)sender {

    [self novoJogo];
    
}

@end
