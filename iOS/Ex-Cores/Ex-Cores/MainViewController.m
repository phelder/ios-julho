//
//  MainViewController.m
//  Ex-Cores
//
//  Created by Helder Pereira on 07/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "ShowTextViewController.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewBack;

@property (weak, nonatomic) IBOutlet UIView *viewPreview;

@property (weak, nonatomic) IBOutlet UILabel *labelRedValue;
@property (weak, nonatomic) IBOutlet UILabel *labelGreenValue;
@property (weak, nonatomic) IBOutlet UILabel *labelBlueValue;

@property (weak, nonatomic) IBOutlet UISlider *sliderRed;
@property (weak, nonatomic) IBOutlet UISlider *sliderGreen;
@property (weak, nonatomic) IBOutlet UISlider *sliderBlue;

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@property (weak, nonatomic) IBOutlet UIButton *buttonGo;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self actualizaCor];
}

- (void)actualizaCor {
    
    float red = self.sliderRed.value;
    float green = self.sliderGreen.value;
    float blue = self.sliderBlue.value;
    
    UIColor *novaCor = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    UIColor *backCor = [UIColor colorWithRed:red green:green blue:blue alpha:0.3];
    
    self.viewPreview.backgroundColor = novaCor;
    
    int redDecimal = red * 255;
    int greenDecimal = green * 255;
    int blueDecimal = blue * 255;
    
    self.labelRedValue.text = [NSString stringWithFormat:@"%d/255", redDecimal];
    self.labelGreenValue.text = [NSString stringWithFormat:@"%d/255", greenDecimal];
    self.labelBlueValue.text = [NSString stringWithFormat:@"%d/255", blueDecimal];
    
    self.textField1.tintColor = novaCor;
    self.textField1.textColor = novaCor;
    
    self.buttonGo.tintColor = novaCor;
    
    self.viewBack.backgroundColor = backCor;
}

#pragma mark - UISlider Actions

- (IBAction)changedSlider:(id)sender {
    
    [self actualizaCor];
    
}

#pragma mark - UIButton Actions

- (IBAction)clickedGo:(id)sender {
    
    [self performSegueWithIdentifier:@"MainToShowText" sender:nil];
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"MainToShowText"]) {
    
        ShowTextViewController *stvc = segue.destinationViewController;
        
        stvc.texto1 = self.textField1.text;
        stvc.cor1 = self.viewPreview.backgroundColor;
    }

}


@end
