//
//  EditarAmigoViewController.m
//  I23-ListaAmigosCoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "EditarAmigoViewController.h"
#import "AppDelegate.h"
#import "Amigo+CoreDataClass.h"

@interface EditarAmigoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageViewFoto;
@property (weak, nonatomic) IBOutlet UITextField *textFieldNome;
@property (weak, nonatomic) IBOutlet UITextField *textFieldIdade;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCidade;

@end

@implementation EditarAmigoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.amigo) {
    
        self.textFieldNome.text = self.amigo.nome;
        self.textFieldIdade.text = [NSString stringWithFormat:@"%d", self.amigo.idade];
        self.textFieldCidade.text = self.amigo.cidade;
    }
}

#pragma mark - UIButton Actions

- (IBAction)clickedTiraFoto:(id)sender {
    
}

- (IBAction)clickedEscolheFoto:(id)sender {
    
}

- (IBAction)clickedGuardar:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    if (!self.amigo) {
        self.amigo = [NSEntityDescription insertNewObjectForEntityForName:@"Amigo" inManagedObjectContext:context];
    }

    self.amigo.nome = self.textFieldNome.text;
    self.amigo.idade = self.textFieldIdade.text.intValue;
    self.amigo.cidade = self.textFieldCidade.text;
    
    [appDelegate saveContext];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
