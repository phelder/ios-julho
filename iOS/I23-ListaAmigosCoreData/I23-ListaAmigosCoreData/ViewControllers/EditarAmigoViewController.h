//
//  EditarAmigoViewController.h
//  I23-ListaAmigosCoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Amigo;

@interface EditarAmigoViewController : UIViewController

@property (strong, nonatomic) Amigo *amigo;

@end
