//
//  AmigosTableViewController.m
//  I23-ListaAmigosCoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "AmigosTableViewController.h"
#import "Amigo+CoreDataClass.h"
#import "AppDelegate.h"
#import "EditarAmigoViewController.h"

@interface AmigosTableViewController ()

@end

@implementation AmigosTableViewController
{
    NSArray <Amigo *> *_amigos;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)readFromCoreData {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    _amigos = [context executeFetchRequest:[Amigo fetchRequest] error:nil];
}

#pragma mark - UIButton Actions

- (IBAction)clickedAdicionar:(id)sender {
    
    [self performSegueWithIdentifier:@"AmigosTableToEditarAmigo" sender:nil];
    
}



#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    [self readFromCoreData];
    
    return _amigos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath];
    
    cell.textLabel.text = _amigos[indexPath.row].nome;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"AmigosTableToEditarAmigo" sender:_amigos[indexPath.row]];

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    [context deleteObject:_amigos[indexPath.row]];
    
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [appDelegate saveContext];
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"AmigosTableToEditarAmigo"]) {
        if (sender != nil) {
            
            EditarAmigoViewController *eavc = segue.destinationViewController;
            eavc.amigo = sender;
        }
    }
    
    
}


@end
