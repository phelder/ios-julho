//
//  Amigo+CoreDataProperties.m
//  I23-ListaAmigosCoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Amigo+CoreDataProperties.h"

@implementation Amigo (CoreDataProperties)

+ (NSFetchRequest<Amigo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Amigo"];
}

@dynamic nome;
@dynamic idade;
@dynamic cidade;

@end
