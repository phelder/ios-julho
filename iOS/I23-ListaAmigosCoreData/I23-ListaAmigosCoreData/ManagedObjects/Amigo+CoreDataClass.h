//
//  Amigo+CoreDataClass.h
//  I23-ListaAmigosCoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Amigo : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Amigo+CoreDataProperties.h"
