//
//  Amigo+CoreDataProperties.h
//  I23-ListaAmigosCoreData
//
//  Created by Helder Pereira on 10/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Amigo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Amigo (CoreDataProperties)

+ (NSFetchRequest<Amigo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *nome;
@property (nonatomic) int32_t idade;
@property (nullable, nonatomic, copy) NSString *cidade;

@end

NS_ASSUME_NONNULL_END
