//
//  AppDelegate.h
//  I20-AFCocoaPods
//
//  Created by Helder Pereira on 07/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

