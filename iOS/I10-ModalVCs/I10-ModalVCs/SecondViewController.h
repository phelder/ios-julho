//
//  SecondViewController.h
//  I10-ModalVCs
//
//  Created by Helder Pereira on 26/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (strong, nonatomic) NSString *texto1;

@end
