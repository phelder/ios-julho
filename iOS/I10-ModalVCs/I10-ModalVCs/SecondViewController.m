//
//  SecondViewController.m
//  I10-ModalVCs
//
//  Created by Helder Pereira on 26/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label1.text = self.texto1;
}

- (IBAction)clickedBack:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
