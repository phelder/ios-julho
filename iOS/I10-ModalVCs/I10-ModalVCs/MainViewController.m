//
//  MainViewController.m
//  I10-ModalVCs
//
//  Created by Helder Pereira on 26/09/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "SecondViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    NSString *textoQualquer = @"Um texto qualquer";
    
    SecondViewController *svc = segue.destinationViewController;
    svc.texto1 = textoQualquer;
    
}


@end
