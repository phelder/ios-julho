//
//  ViewController.m
//  I15-LerEscreverFicheiros
//
//  Created by Helder Pereira on 04/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textView1.layer.borderWidth = 1;
    self.textView1.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.textView1.layer.cornerRadius = 5;
    
}

#pragma mark - UIButton Actions

- (NSURL *)documentsFolderURL {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray <NSURL *> *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    
    NSURL *docURL = urls[0];
    
    return docURL;
}

- (IBAction)clickedEscrever:(id)sender {
    
    NSURL *file = [self.documentsFolderURL URLByAppendingPathComponent:@"ficheiro1.txt"];
    
    [self.textView1.text writeToURL:file atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
}

- (IBAction)clickedLer:(id)sender {
    
    NSURL *file = [self.documentsFolderURL URLByAppendingPathComponent:@"ficheiro1.txt"];
    
    self.textView1.text = [NSString stringWithContentsOfURL:file encoding:NSUTF8StringEncoding error:nil];
    
}

@end
